#!/bin/sh

set -eu

. ./salsarc

if [ "$#" -ne 2 ] || [ -z "$1" ] || [ -z "$2" ]; then
  echo "Usage: $0 https://anonscm.alioth.debian.org/git/pkg-foo/bar.git pkg-baz" >&2
  echo "  where 'pkg-baz' is the name of the Salsa group which will host the new git" >&2
  echo "  repo for 'bar'" >&2
  exit 1
fi

OLD_REPO_URL="$1"
SALSA_GROUP="$2"
REPO_NAME="${OLD_REPO_URL##*/}"
PROJECT="${REPO_NAME%.git}"
DESCRIPTION="$PROJECT packaging"

SALSA_GROUP_ID=$(curl -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/groups?all_available=false" | jq ".[] | select(.path == \"$SALSA_GROUP\") | .id")

curl "$SALSA_URL/projects?private_token=$SALSA_TOKEN" \
  --data "path=$PROJECT&namespace_id=$SALSA_GROUP_ID&description=$DESCRIPTION&import_url=$OLD_REPO_URL&visibility=public"
if [ $? -ne 0 ]; then
    echo
    echo "Something went wrong!"
else
    echo
    echo "You may now browse your project on: ${SALSA_URL%/api*}/$SALSA_GROUP/$PROJECT"
fi
