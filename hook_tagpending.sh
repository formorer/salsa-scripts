#!/bin/sh

set -eu

. ./salsarc

if [ "$#" -ne 1 ] || [ -z "$1" ]; then
    echo "Usage: $0 foo" >&2
    echo "  where 'foo' is the Salsa project name for which you want to setup the webhook" >&2
    exit 1
fi

PROJECT_NAME="$1"
TAGPENDING_URL="https://webhook.salsa.debian.org/tagpending/$PROJECT_NAME"

PROJECT_ID=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" "$SALSA_URL/projects?membership=true&simple=true&search=$PROJECT_NAME" | jq ".[] | select(.name == \"$PROJECT_NAME\") | .id")
if [ -z "$PROJECT_ID" ]; then
    echo "Project $PROJECT_NAME not found among your owned projects on $SALSA_URL service" >&2
    exit 1
else
    echo "Setting up tagpending webhook for $PROJECT_NAME ($PROJECT_ID)"
fi

ALREADY_HOOKED=$(curl --silent -f -XGET --header "PRIVATE-TOKEN: $SALSA_TOKEN" $SALSA_URL/projects/$PROJECT_ID/hooks | jq ".[] | select(.url == \"$TAGPENDING_URL\") | .id")

case $ALREADY_HOOKED in
    [0-9]*) echo "tagpending service is already configured for $PROJECT_NAME" ;;
    *) curl -XPOST --header "PRIVATE-TOKEN: $SALSA_TOKEN" \
	$SALSA_URL/projects/$PROJECT_ID/hooks \
	--data "url=$TAGPENDING_URL&push_events=1&tag_push_events=1&merge_requests_events=1&enable_ssl_verification=1";
       echo;
       if [ $? -eq 0 ]; then
           echo "All done."
       else
           echo "Something went wrong!"
       fi;;
esac
