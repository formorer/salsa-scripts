For the lazy people like me, here are a few simple scripts to interact with
gitlab's API and setup projects on Salsa (salsa.debian.org).

* import.sh https://anonscm.alioth.debian.org/git/pkg-foo/bar.git pkg-baz

  Creates a new project 'bar' in group 'pkg-baz' from an old git repo hosted
in 'https://anonscm.alioth.debian.org/git/pkg-foo/bar.git'

* irker.sh foo \#debian-bar

  Sets up IRC notifications for project 'foo' on IRC channel '#debian-bar'

* hook_tagpending.sh foo

  Sets up a webhook for project 'foo' which marks bug appearing in commit
messages as 'pending' on Debian BTS.

* hook_close.sh foo

  Sets up a webhook for project 'foo' which closes Debian BTS bugs appearing
in commit messages.

* emails_on_push.sh foo "email addresses"

  Sets up email notifications on push for project 'foo'. Emails will be sent
  to "mail addresses".

In order to use these scripts, you must create a 'salsarc' file with the
following content:

	SALSA_URL="https://salsa.debian.org/api/v4"
	SALSA_TOKEN="...."

You may create API tokens by visiting your [personal access tokens](https://salsa.debian.org/profile/personal_access_tokens)
page.

Documentation of Salsa service can be found [here](https://wiki.debian.org/Salsa/Doc)

blah blah